let baseInfo = {"name": "Maciek", "surname": "Muchowski", "email": "maciej.muchowski@gmail.com", "twitter": "none"};

let projects = [
    {
        "projectName": "Projekt 1",
        "projectDetails": "Tu powinienem dać lorem ipsum"
    },
    {
        "projectName": "Projekt 2",
        "projectDetails": "Tu także"
    }
    ];

let cv = {"baseInformation": baseInfo, "projects": [projects]};

function populateName() {
    populate(cv.baseInformation.name, "name")
}

function populateSurname() {
    populate(cv.baseInformation.surname, "surname")
}

function populateEmail() {
    populate(cv.baseInformation.email, "email")
}

function populateDateOfBirth() {
    populate(cv.baseInformation.twitter, "twitter")
}
function populateProjects() {
    let projectsTextTable = [];
    for(let i in cv.projects){
        for (let j in cv.projects[i]) {
            projectsTextTable.push("Nazwa projektu: " + cv.projects[i][j].projectName);
            projectsTextTable.push("Opis projektu: " + cv.projects[i][j].projectDetails);
        }
    }
    populate(projectsTextTable.join("\n"), "projects");
}
function populate(data, targetId) {
    $("#" + targetId).text(data);
}
function populateCvData() {
    populateName();
    populateSurname();
    populateEmail();
    populateDateOfBirth();
    populateProjects();
}

function loadAboutData() {
    $("#websiteContent").load("about-me.html", function () {
        populateCvData();
    });
}

$("#aboutButton").click(loadAboutData);

function loadContactData() {
    $("#websiteContent").load("contact.html");

}

$("#contactButton").click(loadContactData);

function loadProjectData() {
    $("#websiteContent").load("project.html");

}

$("#projectButton").click(loadProjectData);

loadProjectData();